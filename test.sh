#!/bin/bash
#Raigo
#Jagab etteantud grupile kausta
#1.0
export LC_ALL=C
#lisab globaalse muutuja

#kontollime kas skript on k2ivitatud sudos
if [ $EUID -ne 0 ]
then
	echo 'Pole root!'
	exit 1;
fi

#kontrollime et kas muutujaid on piisavalt
if [ $# -eq 2 ]; then
	KAUST=$1
	GROUP=$2
	SHARE=$(basename $KAUST)
	else
	if  [ $# -eq 3 ]; then
		KAUST=$1
		GROUP=$2
		SHARE=$3
		else
			echo "kasutaja skripti $(basename $0) KAUST GRUPP [share]"
			exit 1;
		fi
fi

#path kontroll 
if [ ${KAUST:0:1} == '/' ]; then
	KAUST=$KAUST
else
	KAUST=$(pwd)/$KAUST
fi

#samba kontroll
type smbd > /dev/null 2>&1

if [ $? -ne 0 ]; then
	apt-get update && apt-get install samba -y
else
	echo 'Samba is installed'
fi

#kausta kontroll
test -d $KAUST || mkdir -p $KAUST

#grupi kontroll
getent group $GRUPP > /dev/null || addgroup $GRUPP > /dev/null

#seadistamine samba
mkdir -p /var/data/lab

#samba confi koopia
sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.old

cat >> /etc/samba/smb.conf.old  << EOF

[$SHARE]
	comment=share folder
	path=$KAUST
	writable=yes
	valid user=@$GROUP
	force group=$GROUP
	browseable=yes
	create mask=0664
	directory mask=0775
EOF
testparm -s /etc/samba/smb.conf.old
if [ $? -ne 0 ]; then
	 echo 'Imnes viga'
	exit 1;
fi
cp /etc/samba/smb.conf.old /etc/samba/smb.conf

/etc/init.d/smbd reload

echo 'Scripti l6pp'

